% Created by Postnov D.D. 07.04.2014
% Sniffers,buzzers,toggles and blinkers: dynamics of regulatory and
% signaling patways in the cell
% John J Tyson e t.c.
% Negative-feedback oscillator

function S = RunModel(S, worker)

    S.Hem.pIn = S.Hem.pIn + (worker - 1) / 2;
    timeCounter = 0;
    disp(['Worker ', num2str(worker)]);
    sol = ode23s(@diff_equ, S.Model.tSpan, S.Model.yZero);
    y = deval(sol, S.Model.t);
    S.Res.y = y;

    % Pel & Pact yields [kPa]%
    % Equation 27
    function result = Pel(x)
        result = (1.6 * ((x) - 1) + 6e-3 * exp(10 * ((x) - 0.8)));
    end

    % Equation 28
    function result = Pact(x)
        result = (6.3 + 7.2 * (x) + 4.7 / (1 + exp(13 * (0.4 - (x)))));
    end

    function yPrime = diff_equ(t, y)
        % t: timespan
        % y: initial conditions y0
       
        yPrime = zeros(S.Tree.nephronsN * S.Tree.nephronEq + S.Tree.nodesN * S.Tree.nodeEq, 1);
        
        %nephron equations are first so no equations to skip
        previousEq = 0;
        
        for k = 1:1:S.Tree.nephronsN
            % Equation 25
            psiLocal2(k) = 0;
            psiLocal2(k) = S.Neph.psiMax - (S.Neph.psiMax - S.Neph.psiMin) / (1.0 + (S.Neph.psiEq - S.Neph.psiMin) / (S.Neph.psiMax - S.Neph.psiEq) * exp(S.Neph.alpha * (3 * y(previousEq + (k - 1) * S.Tree.nephronEq + 6) / S.Neph.t / S.Neph.fHenEq - 1.0)));
        end
        
        for k = 1:1:S.Tree.nephronsN
            
            %node connected to current nephron
            cnidx = S.Tree.nephronsMatrix(k, 2);
            
            if (S.Tree.nodesN > 0)
                % Intial value (y0) for pIn
                S.Neph.S.Hem.pIn = y(S.Tree.nephronsN * S.Tree.nephronEq + (cnidx - 1) * S.Tree.nodeEq + 1, 1);
                cvidx = S.Tree.nephronsMatrix(k, 3);
                S.Neph.beta = 0.64; % (S.Hem.length(cvidx)-40)/S.Hem.length(cvidx);
                S.Neph.rAffEq = S.Hem.resistance(cvidx) * 1000; % convert to kPa/s;
            end
            
            psiLocal = psiLocal2(k);
            
            % Equation 29
            pEq = Pel(y(previousEq + (k - 1) * S.Tree.nephronEq + 2)) + psiLocal * Pact(y(previousEq + (k - 1) * S.Tree.nephronEq + 2));
            
            % Equation 16
            rA = S.Neph.rAffEq * (S.Neph.beta + (1.0 - S.Neph.beta) / ((y(previousEq + (k - 1) * S.Tree.nephronEq + 2)) ^ 4));
            
            % Equation 17
            r = S.Neph.rEff / rA;
            
            % Equations 18-21
            tmpA = S.Neph.b + r * S.Neph.b * S.Neph.ha;
            tmpB = S.Neph.a + r * S.Neph.b * S.Neph.cAff * (1 - S.Neph.ha) + r * S.Neph.a * S.Neph.ha;
            tmpC = y(previousEq + (k - 1) * S.Tree.nephronEq + 1) - S.Neph.pEf + r * S.Neph.a * S.Neph.cAff * (1.0 - S.Neph.ha) + r * (y(previousEq + (k - 1) * S.Tree.nephronEq + 1) - S.Neph.S.Hem.pIn) * S.Neph.ha;
            tmpD = (y(previousEq + (k - 1) * S.Tree.nephronEq + 1) - S.Neph.S.Hem.pIn) * r * S.Neph.cAff * (1.0 - S.Neph.ha);
            
            try
                ce = Cubic(tmpA, tmpB, tmpC, tmpD);
            catch
                disp('fail')
            end
            
            % Equation 22
            pG = S.Neph.b * (ce ^ 2) + S.Neph.a * ce + y(previousEq + (k - 1) * S.Tree.nephronEq + 1);
            
            % Equation 23
            pAv = (S.Neph.S.Hem.pIn - (S.Neph.S.Hem.pIn - pG) * S.Neph.beta * S.Neph.rAffEq / rA + pG) / 2;
            
            % Equation 24
            psFun = (1.0 - S.Neph.ha) * (1.0 - S.Neph.cAff / ce) * (S.Neph.S.Hem.pIn - pG) / rA;
            
            %Incoming blood flow, rA is hemodynamic resistance, pG is
            %glomerular blood pressure, pIn is arterial blood pressure
            G = (S.Neph.S.Hem.pIn - pG) / rA; % flow in afferent areteriole
            
            %jumps to node equation (previousEq=S.Tree.nephronsN * S.Tree.nephronEq)
            %affects the flow in the previous node, see equation 4
            %equation 3, yprime - nephron flow, afferent node
            if (S.Tree.nodesN > 0)
                yPrime(S.Tree.nephronsN * S.Tree.nephronEq + (cnidx - 1) * S.Tree.nodeEq + 1, 1) = yPrime(S.Tree.nephronsN * S.Tree.nephronEq + (cnidx - 1) * S.Tree.nodeEq + 1, 1) - G; % important for tree
            end
            
            % Equation 8
            yPrime(previousEq + (k - 1) * S.Tree.nephronEq + 1, 1) = 1.0 / S.Neph.cTub * (psFun - S.Neph.fReab - (y(previousEq + (k - 1) * S.Tree.nephronEq + 1) - S.Neph.pDist) / S.Neph.rHen);
            % Equation 9
            yPrime(previousEq + (k - 1) * S.Tree.nephronEq + 2, 1) = y(previousEq + (k - 1) * S.Tree.nephronEq + 3);
            % Equation 10
            yPrime(previousEq + (k - 1) * S.Tree.nephronEq + 3, 1) = (pAv - pEq) / S.Neph.omega - S.Neph.d * y(previousEq + (k - 1) * S.Tree.nephronEq + 3);
            % Equation 11
            yPrime(previousEq + (k - 1) * S.Tree.nephronEq + 4, 1) = (y(previousEq + (k - 1) * S.Tree.nephronEq + 1) - S.Neph.pDist) / S.Neph.rHen - 3 * y(previousEq + (k - 1) * S.Tree.nephronEq + 4) / S.Neph.t;
            % Equation 12
            yPrime(previousEq + (k - 1) * S.Tree.nephronEq + 5, 1) = 3 * (y(previousEq + (k - 1) * S.Tree.nephronEq + 4) - y(previousEq + (k - 1) * S.Tree.nephronEq + 5)) / S.Neph.t;
            % Equation 13
            yPrime(previousEq + (k - 1) * S.Tree.nephronEq + 6, 1) = 3 * (y(previousEq + (k - 1) * S.Tree.nephronEq + 5) - y(previousEq + (k - 1) * S.Tree.nephronEq + 6)) / S.Neph.t;
        end
        
        %reset previous equation to skip number of nephron equations 
        %calculate node equations
        previousEq = S.Tree.nephronsN * S.Tree.nephronEq;
        for k = 1:1:S.Tree.nodesN
            
            cn1idx = S.Tree.nodesMatrix(k, 11);
            cn2idx = S.Tree.nodesMatrix(k, 12);
            cn3idx = S.Tree.nodesMatrix(k, 13);
            
            cv1idx = S.Tree.nodesMatrix(k, 14);
            cv2idx = S.Tree.nodesMatrix(k, 15);
            cv3idx = S.Tree.nodesMatrix(k, 16);
          
            
            
            %Find the number of following nodes, if the current node is an
            %afferent node, the values are set equal to the current node
            %and cancelled out in equation 4, as it has already been
            %modelled in equation 3
            
            % Parent node
            if (S.Tree.nodesMatrix (k, 3) == - 1)
                cp1 = S.Hem.pIn;
            else
                cp1 = y(previousEq + (cn1idx - 1) * S.Tree.nodeEq + 1);
            end
            
            %Daughter node 1
            if (S.Tree.nodesMatrix (k, 4) == - 1)
                cp2 = - y(previousEq + (k - 1) * S.Tree.nodeEq + 1);
            else
                cp2 = y(previousEq + (cn2idx - 1) * S.Tree.nodeEq + 1);
            end
            
            %Daughter node 2
            if (S.Tree.nodesMatrix (k, 5) == - 1)
                cp3 = - y(previousEq + (k - 1) * S.Tree.nodeEq + 1);
            else
                cp3 = y(previousEq + (cn3idx - 1) * S.Tree.nodeEq + 1);
            end
            
            
            % implement myogenic response here to change the resistance
            % dependent on vessel diameter
            % update diameter, resistance and viscosity in the s model
            % based on the myogenic activation
            %Pressure: S.Hem.pIn
            %Flow must be the first term of equation 4
            % Resistance: S.Hem.resistance(cv1idx)
            %Following equations numbers refer to: Dynamic myogenic autoregulation in the rat kidney: a whole-organ model
            
            
            %Solving for diameter
            fun = @diameter_eq;
            x0 = y(previousEq + (k - 1) * S.Tree.nodeEq + 3, 1);%S.Hem.anatomic_diameter(cv1idx);
            options = optimset('Display','off');
            %S.Hem.diameter(cv1idx) = lsqnonlin(fun,x0,0,[],options);
            S.Hem.diameter(cv1idx) = fsolve(fun,x0,options);
            
            S.Hem.diameter(cv1idx);
            


            %Unit: Centipoise (=mPa*s)
            S.Hem.visc(cv1idx) = ((1 + (6 * exp(- 0.085 * S.Hem.diameter(cv1idx)) + 2.2 - 2.44 ...
                * exp(- 0.06 * (S.Hem.diameter(cv1idx) ^ 0.645))) ...
                * ((S.Hem.diameter(cv1idx) / (S.Hem.diameter(cv1idx) - 1.1)) ^ 2)) ...
                * ((S.Hem.diameter(cv1idx) / (S.Hem.diameter(cv1idx) - 1.1)) ^ 2));


            %Unit conversion: kPa*s
            S.Hem.visc(cv1idx) = S.Hem.visc(cv1idx) / 1e6;

            %Unit: kPa*s/micro cubic meters
            S.Hem.resistance(cv1idx)    = (128 * S.Hem.visc(cv1idx) * S.Hem.length(cv1idx)) / (pi * (S.Hem.diameter(cv1idx) ^ 4));

            P_i = (cp1 - (y(1)+y(7)));
            %P_i = (cp1 - y(previousEq + (k - 1) * S.Tree.nodeEq + 1));            
            %Unit: micro cubic meters / second
            Q = (P_i) / S.Hem.resistance(cv1idx);



            dnorm = S.Hem.diameter(cv1idx)/ S.Hem.anatomic_diameter(cv1idx);

            %Equation 7
            %Unit: N/m
            Tpass = S.Hem.Cp(cv1idx) * exp(S.Hem.Cp2(cv1idx) * (dnorm - 1));

            %Equation 8
            %Unit: N/m
            Tmax = S.Hem.Ca(cv1idx) * exp(- ((dnorm - S.Hem.Ca2(cv1idx)) / S.Hem.Ca3(cv1idx)) ^ 2);
            %Equation 6
            %Unit: N/m
            Ttot = Tpass + y(previousEq + (k - 1) * S.Tree.nodeEq + 2) * Tmax;

            %Equation 9
            %Unit: kPa
            tau = ((32 * Q * S.Hem.visc(cv1idx)) / (pi * S.Hem.diameter(cv1idx) ^ 3));

            %Equation 10
            %Unit: Unitless (percentage activation)
            phiNo = 1 / (1 + exp(- (S.Hem.CNo(cv1idx) * tau - S.Hem.CNo2(cv1idx))));

            %Equation 11
            %Unit: Unitless (percentage activation)
            phiMRss = 1 / (1 + exp(- ((S.Hem.Ct(cv1idx) * Ttot) / (1 + S.Hem.alfa(cv1idx) * phiNo) - S.Hem.Ct2(cv1idx))));

            
            %Equation 4 (Dmitry article)
            yPrime (previousEq + (k - 1) * S.Tree.nodeEq + 1, 1) = yPrime (previousEq + (k - 1) ...
                * S.Tree.nodeEq + 1, 1) + ((cp1 - y(previousEq + (k - 1) * ...
                S.Tree.nodeEq + 1)) / S.Hem.resistance(cv1idx) - (y(previousEq + (k - 1) *...
                S.Tree.nodeEq + 1) - cp2) / S.Hem.resistance(cv2idx) - (y(previousEq + (k - 1) *...
                S.Tree.nodeEq + 1) - cp3) / S.Hem.resistance(cv3idx)) / S.Hem.elast;

            
            % Equation 13 (MR activation)
            yPrime(previousEq + (k - 1) * S.Tree.nodeEq + 2, 1) = 1 / S.Hem.ta(cv1idx) * (phiMRss - y(previousEq + (k - 1) * S.Tree.nodeEq + 2));
            % Diameter
            yPrime(previousEq + (k - 1) * S.Tree.nodeEq + 3, 1) = S.Hem.diameter(cv1idx) - y(previousEq + (k - 1) * S.Tree.nodeEq + 3);
            % Flow
            yPrime(previousEq + (k - 1) * S.Tree.nodeEq + 4, 1) = Q - y(previousEq + (k - 1) * S.Tree.nodeEq + 4);
            %yPrime(3,1) = Tmax - y(3);
            %yPrime(5,1)= S.Hem.visc(cv1idx) - y(5);
        end
        
        function F = diameter_eq(diameter)
          
       
            visc = ((1 + (6 * exp(- 0.085 * diameter) + 2.2 - 2.44 ...
            * exp(- 0.06 * (diameter ^ 0.645))) ...
            * ((diameter / (diameter - 1.1)) ^ 2)) ...
            * ((diameter / (diameter - 1.1)) ^ 2));


            %Unit conversion: kPa*s
            visc = visc / 1e6;

            %Unit: Pa*s/micro cubic meters
            resistance    = (128 * visc * S.Hem.length(1)) / (pi * (diameter ^ 4));
            P_i = (cp1 - (y(1)+y(7)));
            %P_i = (cp1 - y(previousEq + (k - 1) * S.Tree.nodeEq + 1));
            
            %Flow
            %Unit: micro cubic meters / second
            Q = (P_i) / resistance;


            dnorm = diameter/ S.Hem.anatomic_diameter(cv1idx);

            %Equation 7
            %Unit: N/m
            Tpass = S.Hem.Cp(cv1idx) * exp(S.Hem.Cp2(cv1idx) * (dnorm - 1));

            %Equation 8
            %Unit: N/m
            Tmax = S.Hem.Ca(cv1idx) * exp(- ((dnorm - S.Hem.Ca2(cv1idx)) / S.Hem.Ca3(cv1idx)) ^ 2);
            %Equation 6
            %Unit: N/m
            Ttot = Tpass + y(previousEq + (k - 1) * S.Tree.nodeEq + 2) * Tmax;

            %Equation 9
            %Unit: Pa
            %vessel_no=2.254e5 * exp(-0.101*S.Hem.anatomic_diameter(cv1idx));
            vessel_no=1;
            tau = (32 * Q * visc / (vessel_no*pi * diameter ^ 3));

            %Equation 10
            %Unit: Unitless (percentage activation)
            phiNo = 1 / (1 + exp(- (S.Hem.CNo(cv1idx) * tau - S.Hem.CNo2(cv1idx))));

            %Equation 11
            %Unit: Unitless (percentage activation)
            phiMRss = 1 / (1 + exp(- ((S.Hem.Ct(cv1idx) * Ttot) / (1 + S.Hem.alfa(cv1idx) * phiNo) - S.Hem.Ct2(cv1idx))));
            
            %Equation 3
            P_i2=(cp1 + y(previousEq + (k - 1) * S.Tree.nodeEq + 1))/2;
            
            %Equation 12
            F = (S.Hem.Cp(cv1idx)  * exp(S.Hem.Cp2(cv1idx) * (dnorm - 1)) + S.Hem.Ca(cv1idx) * phiMRss * exp(-((dnorm - S.Hem.Ca2(cv1idx)) / S.Hem.Ca3(cv1idx))^2))*10^6 - (P_i2 * diameter / 2);
        end
            
        
            
    end
    

    function [x, y] = Impow13(x, y)
        impowv = atan2(y, x) / 3.0;
        impowr = hypot(x, y) ^ (1.0 / 3.0);
        y = sin(impowv) * impowr;
        x = cos(impowv) * impowr;
    end

    function result = Cubic(s, p, q, r) % This function is mostly CRAP. Works only in our case (but taken from original works and used in S.Neph.a lot of papers). Better rewrite it when i have time
        i = 2;
        if (s ~= 0.0)
            p = p / s;
            q = q / s;
            r = r / s;
            aa = (3.0 * q - p * p) / 3.0;
            bb = (2.0 * p * p * p - 9 * p * q + 27.0 * r) / 27.0;
            dummy = bb * bb / 4.0 + aa * aa * aa / 27.0;
            if (dummy >= 0.0)
                A = (- bb) / 2.0 + sqrt(dummy);
                Aim = 0.0;
                B = (- bb) / 2.0 - sqrt(dummy);
                Bim = 0.0;
                if (A < 0.0)
                    A = 0.0 - (0.0 - A) ^ (1.0 / 3.0);
                else
                    A = A ^ (1.0 / 3.0);
                end
                if (B < 0.0)
                    B = 0.0 - (0.0 - B) ^ (1.0 / 3.0);
                else
                    B = B ^ (1.0 / 3.0);
                end
            else
                A = (- bb) / 2.0;
                Aim = sqrt(0.0 - dummy);
                B = (- bb) / 2.0;
                Bim = 0.0 - sqrt(0.0 - dummy);
                [A, Aim] = Impow13(A, Aim);
                [B, Bim] = Impow13(B, Bim);
            end
           
            res(1) = A + B - p / 3;
            res(2) = (- (A + B)) / 2.0 - sqrt(3.0) * (Aim - Bim) / 2.0 - p / 3;
            res(3) = (- (A + B)) / 2.0 + sqrt(3.0) * (Aim - Bim) / 2.0 - p / 3;
            result = res(1);
        end
    end
    function result = Cubic2(A,B,C,D)
       result=A*x^3+B*x^2+C*x+D; 
    end
    

end
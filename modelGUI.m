function modelGUI(Res)
steps=size(Res,1);
modelFig=figure('Visible', 'on', 'Position', [150, 250, 1000, 650],...
        'Name','-- Model Inspector  --');

graphPanel = uitabgroup(modelFig,'Position',[.025,.025,.95,.95]);


%% initiations
colorMatrix={'red','blue','green','magenta','cyan'};


%% Graph of Diameter

tab1 = uitab(graphPanel,'Title','Pressure');

preGraph= axes('Parent',tab1,'Units','Pixels','Position',[40,40,650,500]);

%% Graph of response

tab2 = uitab(graphPanel,'Title','Response');

resGraph=axes('Parent',tab2,'Units','Pixels','Position',[40,40,650,500]);
%% Graph of diameters
tab3 = uitab(graphPanel,'Title','Diameter');

diaGraph=axes('Parent',tab3,'Units','Pixels','Position',[40,40,650,500]);

uicontrol('Parent',tab3,'Style','text','String','# Initial pressures:',...
            'Position',[770 530 100 20],'Background','white');
uicontrol('Parent',tab3,'Style','text','String',num2str(size(Res,1)),...
            'Position',[890 530 20 20],'Background','white');
uichangepres2= uicontrol('Parent',tab3,'Style', 'popup',...
           'Position', [770 480 150 30],...
           'Callback', @changepres2);   
%% Options Pressure
%optionPanel = uipanel('Title','Options','Fontsize',10,'Position',[.775,.025,.2,.95]);
uicontrol('Parent',tab1,'Style','text','String','# Initial pressures:',...
            'Position',[770 530 100 20],'Background','white');
uicontrol('Parent',tab1,'Style','text','String',num2str(size(Res,1)),...
            'Position',[890 530 20 20],'Background','white');
uichangepres= uicontrol('Parent',tab1,'Style', 'popup',...
           'Position', [770 480 150 30],...
           'Callback', @changepres);   
%% Options Response
uicontrol('Parent',tab2,'Style','text','String',{'nephron1','nephron2','node','total mean','nephron mean'},...
            'Position',[750 430 120 100],'Background','white','FontSize',18);
btn1 = uicontrol('Parent',tab2,'Style','checkbox','Position',[870 510 20 20],...
    'Value',1);
btn2 = uicontrol('Parent',tab2,'Style','checkbox','Position',[870 490 20 20],...
    'Value',1);
btn3 = uicontrol('Parent',tab2,'Style','checkbox','Position',[870 470 20 20],...
    'Value',1);
btn4 = uicontrol('Parent',tab2,'Style','checkbox','Position',[870 450 20 20],...
    'Value',1);
btn5 = uicontrol('Parent',tab2,'Style','checkbox','Position',[870 430 20 20],...
    'Value',1);
uicontrol('Parent',tab2,'Style','pushbutton','String','Update','Position',...
            [770 410 100 20],'Callback',@updateGraphFn);
%% Initiate
% plot pressure
    plot(linspace(Res(1).Model.tSpan(1),Res(1).Model.tSpan(2),Res(1).Model.points),Res(1).Res.y(1:6:13, :)','Parent',preGraph);
    legend(preGraph,'nephron1','nephron2','node1')
    xlabel('time','Parent',preGraph)
    ylabel('pressure','Parent',preGraph)
%plot diameter
        hold(diaGraph,'on')
        plot(linspace(Res(1).Model.tSpan(1),Res(1).Model.tSpan(2),Res(1).Model.points),Res(1).Res.y(5, :)'*Res(1).Tree.vesselsMatrix(2,4),'Parent',diaGraph);
        plot(linspace(Res(1).Model.tSpan(1),Res(1).Model.tSpan(2),Res(1).Model.points),Res(1).Res.y(11, :)'*Res(1).Tree.vesselsMatrix(3,4),'Parent',diaGraph);
        if size(Res(1).Res.y,1)>13
        plot(linspace(Res(1).Model.tSpan(1),Res(1).Model.tSpan(2),Res(1).Model.points),Res(1).Res.y(15, :)','Parent',diaGraph);
        end
        hold(diaGraph,'off')
% presuretab
string1={''};
for i=1:steps
    string1(i)={num2str(Res(i).Hem.pIn)};
end
set([uichangepres,uichangepres2],'String',string1)
    
% responsetab
meanPres1=zeros(1,steps);
meanPres2=zeros(1,steps);
meanFlow1=zeros(1,steps);
meanFlow2=zeros(1,steps);
meanFlow3=zeros(1,steps);

inPres=[];
for i=1:steps
    meanPres1(i)=mean(Res(i).Res.y(1, round(length(Res(i).Res.y(1,:))*0.3):end));
    meanFlow1(i)=(meanPres1(i)-1.3)/Res(i).Hem.resistance(3);
    meanPres2(i)=mean(Res(i).Res.y(7, round(length(Res(i).Res.y(1,:))*0.3):end));
    meanFlow2(i)=(meanPres2(i)-1.3)/Res(i).Hem.resistance(2);
    meanFlow3(i)=(mean(Res(i).Res.y(13, round(length(Res(i).Res.y(1,:))*0.5):end))-(meanPres1(i)+meanPres2(i)))/Res(i).Hem.resistance(1);
    inPres(i)=Res(i).Hem.pIn;
end
meanFlow4=mean([meanFlow1;meanFlow2;meanFlow3]);
meanFlow5=mean([meanFlow1;meanFlow2]);
hold(resGraph,'on')
plot(resGraph,inPres,meanFlow1,cell2mat(colorMatrix(1)));
plot(resGraph,inPres,meanFlow2,cell2mat(colorMatrix(2)));
plot(resGraph,inPres,meanFlow3,cell2mat(colorMatrix(3)));
plot(resGraph,inPres,meanFlow5,cell2mat(colorMatrix(4)));%nephron mean
plot(resGraph,inPres,meanFlow4,cell2mat(colorMatrix(5)));%overal mean
legend(resGraph,'nephron1','nephron2','node','nephron mean','total mean')
hold(resGraph,'on')

    xlabel('Initial Pressure(kPa)','Parent',resGraph)
    ylabel('Flow(\mu L /s)','Parent',resGraph)
%% functions
    function changepres(source,eventdata)
        plot(linspace(Res(1).Model.tSpan(1),Res(1).Model.tSpan(2),Res(1).Model.points),Res(source.Value).Res.y(1:6:13, :)','Parent',preGraph);
        
    end
    function changepres2(source,eventdata)
        cla(diaGraph)
        hold(diaGraph,'on')
        plot(linspace(Res(1).Model.tSpan(1),Res(1).Model.tSpan(2),Res(1).Model.points),Res(source.Value).Res.y(5, :)'*Res(source.Value).Tree.vesselsMatrix(2,4),'Parent',diaGraph);
        plot(linspace(Res(1).Model.tSpan(1),Res(1).Model.tSpan(2),Res(1).Model.points),Res(source.Value).Res.y(11, :)'*Res(source.Value).Tree.vesselsMatrix(3,4),'Parent',diaGraph);
        if size(Res(1).Res.y,1)>13
        plot(linspace(Res(1).Model.tSpan(1),Res(1).Model.tSpan(2),Res(1).Model.points),Res(source.Value).Res.y(15, :)','Parent',diaGraph);
        end
        hold(diaGraph,'off')
    end
    function updateGraphFn(source,eventdata)
        graphs=[get(btn1,'Value'),get(btn2,'Value'),get(btn3,'Value'),get(btn4,'Value'),get(btn5,'Value')];
        cla(resGraph)
        hold(resGraph,'on')
        for j=1:5
           if graphs(j)==1
               plot(resGraph,inPres,eval(strcat('meanFlow',num2str(j))),cell2mat(colorMatrix(j)));
           end
        end
        hold(resGraph,'off')
    end
end
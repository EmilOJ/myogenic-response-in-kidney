function [coeffeicientsX3, pars, vars] = GetX3Matrix(vesselsMatrix, nephronsMatrix)
pars.ec.length = 50.0; % muM - length of endothelial cell
pars.ec.width = 5.0; % muM - width of endothelial cell
pars.ec.restG = 1 / 8; % 1/8.0; % 5-10 MOhm - rest resistance
pars.ec.gapG = 1 / 3; % 1/3.0; %MOhm - gap junctions resistance
pars.ec.c = 20.0; % pF endothelial cell capacity
pars.ec.restV = - 40;

pars.ec.posV = 10.0;
pars.ec.negV = - 75.0;
pars.ec.posR = 10.0 / pars.ec.restG;
pars.ec.negR = - 10.0 / pars.ec.restG;

pars.ec.kirG = 3.0 * pars.ec.restG;
pars.ec.kirDelta = 20.0;
pars.ec.kirH = - 42.0;
pars.ec.kirV = - 80.0;

pars.neph.psi = 1;
pars.neph.k = 1;

vesMatrix = transformMatrix(vesselsMatrix, 'vesselsMatrix');

pars.st.vesselsMatrix = vesMatrix;
pars.st.vesselsN = length(pars.st.vesselsMatrix(:, 1));
pars.st.nephronsMatrix = [2; 3];
% [2;3;29;25;5;6;39;35;19;15;11];
% [1;2;35;31;27;23;19;15;11;7];
% [1;2;31;27;23;19;15;11;7]
% [1;2;23;19;15;11;7];
% [2;3;41;37;33;29;5;7;9;10;67;63;59;55;51;47;23;19;15]
% [2;3;29;25;5;6;39;35;19;15;11];%[3;4;5;47;43;39;35;8;9;73;69;65;11;12;87;83;79;59;55;29;25;21;17];%[3;4;5;47;43;39;35;31;8;9;81;77;73;11;12;99;95;91;87;67;63;59;55;25;21;17];%[2;3];%;29;25;5;6;39;35;19;15;11] ;
pars.st.nephronsN = length(pars.st.nephronsMatrix(:));

pars.st.cellsN = 0;

for j = 1:1:pars.st.vesselsN;
    pars.segment(j).vesselType = 'sub';
    
    if (pars.st.vesselsMatrix(j, 5) == - 1)
        pars.segment(j).parentIdx = - 1;
        pars.segment(j).sisterIdx = - 1;
        pars.segment(j).vesselType = 'root';
    else
        pars.segment(j).parentIdx = find (pars.st.vesselsMatrix(:, 1) == pars.st.vesselsMatrix(j, 5), 1, 'first');
        pars.segment(j).sisterIdx = find (pars.st.vesselsMatrix(:, 1) == pars.st.vesselsMatrix(j, 6), 1, 'first');
    end
    
    if (pars.st.vesselsMatrix(j, 7) == - 1 && pars.st.vesselsMatrix(j, 8) == - 1)
        pars.segment(j).daughter1Idx = - 1;
        pars.segment(j).daughter2Idx = - 1;
        pars.segment(j).vesselType = 'terminal';
        pars.segment(j).nephronIdx = find (pars.st.nephronsMatrix(:, 1) == pars.st.vesselsMatrix(j, 9), 1, 'first');
    else
        pars.segment(j).daughter2Idx = find (pars.st.vesselsMatrix(:, 1) == pars.st.vesselsMatrix(j, 8), 1, 'first');
        pars.segment(j).daughter1Idx = find (pars.st.vesselsMatrix(:, 1) == pars.st.vesselsMatrix(j, 7), 1, 'first');
        pars.segment(j).nephronIdx = - 1;
    end
    
    if (pars.segment(j).daughter1Idx == pars.segment(j).parentIdx)
        oneVesselException = MException('PsiMatrix:badStructure', ' Vessel is root and terminal at the same time');
        throw(oneVesselException);
    end
    
    pars.segment(j).length = pars.st.vesselsMatrix(j, 3);
    pars.segment(j).diameter = pars.st.vesselsMatrix(j, 4);
    
    pars.segment(j).lengthCells = floor(pars.segment(j).length / pars.ec.length);
    if (pars.segment(j).lengthCells == 0)
        pars.segment(j).lengthCells = 1;
    end
    
    pars.segment(j).circumferenceCells = floor((pi * pars.segment(j).diameter) / pars.ec.width);
    if (pars.segment(j).circumferenceCells == 0)
        pars.segment(j).circumferenceCells = 1;
    end
    
    pars.segment(j).firstCellBlock = pars.st.cellsN + 1;
    pars.st.cellsN = pars.st.cellsN + pars.segment(j).lengthCells;
    pars.segment(j).lastCellBlock = pars.st.cellsN;
    
end

for j = 1:1:pars.st.vesselsN;
    
    circCells = double(pars.segment(j).circumferenceCells);
    diameter = pars.segment(j).diameter;
    switch pars.segment(j).vesselType
        case 'root'
            daughter1CircCells = double(pars.segment(pars.segment(j).daughter1Idx).circumferenceCells);
            daughter2CircCells = double(pars.segment(pars.segment(j).daughter2Idx).circumferenceCells);
            
            daughter1Diam = pars.segment(pars.segment(j).daughter1Idx).diameter;
            daughter2Diam = pars.segment(pars.segment(j).daughter2Idx).diameter;
            
            pars.segment(j).Ncd1 = round(((daughter1CircCells / daughter2CircCells) * circCells) / (1 + daughter1CircCells / daughter2CircCells));
            pars.segment(j).Ncd2 = circCells - pars.segment(j).Ncd1;
            if ((daughter1Diam > daughter2Diam && pars.segment(j).Ncd1 < pars.segment(j).Ncd2) || (daughter1Diam < daughter2Diam && pars.segment(j).Ncd1 > pars.segment(j).Ncd2))
                temporaryValue = pars.segment(j).Ncd1;
                pars.segment(j).Ncd1 = pars.segment(j).Ncd2;
                pars.segment(j).Ncd2 = temporaryValue;
            end
            
            pars.segment(j).Nd1c = round(((circCells / daughter2CircCells) * daughter1CircCells) / (1 + circCells / daughter2CircCells));
            pars.segment(j).Nd1d2 = daughter1CircCells - pars.segment(j).Nd1c;
            if ((diameter > daughter2Diam && pars.segment(j).Nd1c < pars.segment(j).Nd1d2) || (diameter < daughter2Diam && pars.segment(j).Nd1c > pars.segment(j).Nd2d1))
                temporaryValue = pars.segment(j).Nd1c;
                pars.segment(j).Nd1c = pars.segment(j).Nd1d2;
                pars.segment(j).Nd1d2 = temporaryValue;
            end
            
            pars.segment(j).Nd2c = round(((circCells / daughter1CircCells) * daughter2CircCells) / (1 + circCells / daughter1CircCells));
            pars.segment(j).Nd2d1 = daughter2CircCells - pars.segment(j).Nd2c;
            if ((diameter > daughter1Diam && pars.segment(j).Nd2c < pars.segment(j).Nd2d1) || (diameter < daughter1Diam && pars.segment(j).Nd2c > pars.segment(j).Nd2d1))
                temporaryValue = pars.segment(j).Nd2c;
                pars.segment(j).Nd2c = pars.segment(j).Nd2d1;
                pars.segment(j).Nd2d1 = temporaryValue;
            end
            
            pars.segment(j).parentGapG = pars.ec.gapG * circCells * 2;
            pars.segment(j).sisterGapG = pars.ec.gapG * circCells * 2;
            
            % ROUND question??????
            pars.segment(j).daughter1GapG = pars.ec.gapG * (pars.segment(j).Ncd1 + pars.segment(j).Nd1c) / 2;
            pars.segment(j).daughter2GapG = pars.ec.gapG * (pars.segment(j).Ncd2 + pars.segment(j).Nd2c) / 2;
            
        case 'terminal'
            parentCircCells = double(pars.segment(pars.segment(j).parentIdx).circumferenceCells);
            sisterCircCells = double(pars.segment(pars.segment(j).sisterIdx).circumferenceCells);
            
            parentDiam = pars.segment(pars.segment(j).parentIdx).diameter;
            sisterDiam = pars.segment(pars.segment(j).sisterIdx).diameter;
            
            pars.segment(j).Ncp = round(((parentCircCells / sisterCircCells) * circCells) / (1 + parentCircCells / sisterCircCells));
            pars.segment(j).Ncs = circCells - pars.segment(j).Ncp;
            
            if ((parentDiam > sisterDiam && pars.segment(j).Ncp < pars.segment(j).Ncs) || (parentDiam < sisterDiam && pars.segment(j).Ncp > pars.segment(j).Ncs))
                temporaryValue = pars.segment(j).Ncp;
                pars.segment(j).Ncp = pars.segment(j).Ncs;
                pars.segment(j).Ncs = temporaryValue;
            end
            
            pars.segment(j).Npc = round(((circCells / sisterCircCells) * parentCircCells) / (1 + circCells / sisterCircCells));
            pars.segment(j).Nps = daughter1CircCells - pars.segment(j).Npc;
            if ((diameter > sisterDiam && pars.segment(j).Npc < pars.segment(j).Nps) || (diameter < sisterDiam && pars.segment(j).Npc > pars.segment(j).Nps))
                temporaryValue = pars.segment(j).Npc;
                pars.segment(j).Npc = pars.segment(j).Nps;
                pars.segment(j).Nps = temporaryValue;
            end
            
            pars.segment(j).Nsc = round(((circCells / parentCircCells) * sisterCircCells) / (1 + circCells / parentCircCells));
            pars.segment(j).Nsp = daughter2CircCells - pars.segment(j).Nsc;
            if ((diameter > parentDiam && pars.segment(j).Nsc < pars.segment(j).Nsp) || (diameter < parentDiam && pars.segment(j).Nsc > pars.segment(j).Nsp))
                temporaryValue = pars.segment(j).Nsc;
                pars.segment(j).Nsc = pars.segment(j).Nsp;
                pars.segment(j).Nsp = temporaryValue;
            end
            
            pars.segment(j).parentGapG = pars.ec.gapG * (pars.segment(j).Ncp + pars.segment(j).Npc) / 2;
            pars.segment(j).sisterGapG = pars.ec.gapG * (pars.segment(j).Ncs + pars.segment(j).Nsc) / 2;
            pars.segment(j).daughter1GapR = - 1;
            pars.segment(j).daughter2GapR = - 1;
            
        case 'sub'
            parentCircCells = double(pars.segment(pars.segment(j).parentIdx).circumferenceCells);
            sisterCircCells = double(pars.segment(pars.segment(j).sisterIdx).circumferenceCells);
            daughter1CircCells = double(pars.segment(pars.segment(j).daughter1Idx).circumferenceCells);
            daughter2CircCells = double(pars.segment(pars.segment(j).daughter2Idx).circumferenceCells);
            
            daughter1Diam = pars.segment(pars.segment(j).daughter1Idx).diameter;
            daughter2Diam = pars.segment(pars.segment(j).daughter2Idx).diameter;
            
            pars.segment(j).Ncd1 = round(((daughter1CircCells / daughter2CircCells) * circCells) / (1 + daughter1CircCells / daughter2CircCells));
            pars.segment(j).Ncd2 = circCells - pars.segment(j).Ncd1;
            if ((daughter1Diam > daughter2Diam && pars.segment(j).Ncd1 < pars.segment(j).Ncd2) || (daughter1Diam < daughter2Diam && pars.segment(j).Ncd1 > pars.segment(j).Ncd2))
                temporaryValue = pars.segment(j).Ncd1;
                pars.segment(j).Ncd1 = pars.segment(j).Ncd2;
                pars.segment(j).Ncd2 = temporaryValue;
            end
            
            pars.segment(j).Nd1c = round(((circCells / daughter2CircCells) * daughter1CircCells) / (1 + circCells / daughter2CircCells));
            pars.segment(j).Nd1d2 = daughter1CircCells - pars.segment(j).Nd1c;
            if ((diameter > daughter2Diam && pars.segment(j).Nd1c < pars.segment(j).Nd1d2) || (diameter < daughter2Diam && pars.segment(j).Nd1c > pars.segment(j).Nd2d1))
                temporaryValue = pars.segment(j).Nd1c;
                pars.segment(j).Nd1c = pars.segment(j).Nd1d2;
                pars.segment(j).Nd1d2 = temporaryValue;
            end
            
            pars.segment(j).Nd2c = round(((circCells / daughter1CircCells) * daughter2CircCells) / (1 + circCells / daughter1CircCells));
            pars.segment(j).Nd2d1 = daughter2CircCells - pars.segment(j).Nd2c;
            if ((diameter > daughter1Diam && pars.segment(j).Nd2c < pars.segment(j).Nd2d1) || (diameter < daughter1Diam && pars.segment(j).Nd2c > pars.segment(j).Nd2d1))
                temporaryValue = pars.segment(j).Nd2c;
                pars.segment(j).Nd2c = pars.segment(j).Nd2d1;
                pars.segment(j).Nd2d1 = temporaryValue;
            end
            
            % ROUND question??????
            pars.segment(j).daughter1GapG = pars.ec.gapG * (pars.segment(j).Ncd1 + pars.segment(j).Nd1c) / 2;
            pars.segment(j).daughter2GapG = pars.ec.gapG * (pars.segment(j).Ncd2 + pars.segment(j).Nd2c) / 2;
            
            parentDiam = pars.segment(pars.segment(j).parentIdx).diameter;
            sisterDiam = pars.segment(pars.segment(j).sisterIdx).diameter;
            
            pars.segment(j).Ncp = round(((parentCircCells / sisterCircCells) * circCells) / (1 + parentCircCells / sisterCircCells));
            pars.segment(j).Ncs = circCells - pars.segment(j).Ncp;
            
            if ((parentDiam > sisterDiam && pars.segment(j).Ncp < pars.segment(j).Ncs) || (parentDiam < sisterDiam && pars.segment(j).Ncp > pars.segment(j).Ncs))
                temporaryValue = pars.segment(j).Ncp;
                pars.segment(j).Ncp = pars.segment(j).Ncs;
                pars.segment(j).Ncs = temporaryValue;
            end
            
            pars.segment(j).Npc = round(((circCells / sisterCircCells) * parentCircCells) / (1 + circCells / sisterCircCells));
            pars.segment(j).Nps = daughter1CircCells - pars.segment(j).Npc;
            if ((diameter > sisterDiam && pars.segment(j).Npc < pars.segment(j).Nps) || (diameter < sisterDiam && pars.segment(j).Npc > pars.segment(j).Nps))
                temporaryValue = pars.segment(j).Npc;
                pars.segment(j).Npc = pars.segment(j).Nps;
                pars.segment(j).Nps = temporaryValue;
            end
            
            pars.segment(j).Nsc = round(((circCells / parentCircCells) * sisterCircCells) / (1 + circCells / parentCircCells));
            pars.segment(j).Nsp = daughter2CircCells - pars.segment(j).Nsc;
            if ((diameter > parentDiam && pars.segment(j).Nsc < pars.segment(j).Nsp) || (diameter < parentDiam && pars.segment(j).Nsc > pars.segment(j).Nsp))
                temporaryValue = pars.segment(j).Nsc;
                pars.segment(j).Nsc = pars.segment(j).Nsp;
                pars.segment(j).Nsp = temporaryValue;
            end
            
            % ROUND question??????
            pars.segment(j).parentGapG = pars.ec.gapG * (pars.segment(j).Ncp + pars.segment(j).Npc) / 2;
            pars.segment(j).sisterGapG = pars.ec.gapG * (pars.segment(j).Ncs + pars.segment(j).Nsc) / 2;
            
        otherwise
            oneVesselException = MException('PsiMatrix:badStructure', ' Vessel type is not determined');
            throw(oneVesselException);
            
    end
end

vars.nephV = zeros(pars.st.nephronsN, pars.st.nephronsN);
vars.nephI = zeros(pars.st.nephronsN, pars.st.nephronsN);

n = 1;
for j = 1:1:pars.st.vesselsN;
    if strcmp(pars.segment(j).vesselType, 'terminal')
        pars.st.segmentToNeph(n) = j;
        n = n + 1;
    end
end

for n = 1:1:pars.st.nephronsN
    
    vars.ecV0 = zeros(pars.st.cellsN, 1);
    vars.ecV0(:) = pars.ec.restV;
    vars.nephV0 = zeros(pars.st.nephronsN, 1);
    vars.nephV0(:) = pars.ec.restV;
    vars.nephV0(n) = (pars.neph.psi * pars.neph.k + pars.ec.restV); % pars.neph.psi*pars.neph.k*(pars.ec.gapG*double(pars.segment(j).circumferenceCells));
    % vars.nephV(n,n)=10;
    % vars.ecV0(pars.segment( pars.st.segmentToNeph(n)).lastCellBlock)=vars.nephI(n)*(pars.ec.gapR/pars.segment( pars.st.segmentToNeph(n)).circumferenceCells);
    
    rungeH = 0.1;
    tt = 0;
    rungeY = vars.ecV0;
    rungeStep = 1;
    rungePoint = 1;
    rungeToOut = 100;
    rungeTime = 1000;
    while tt < rungeTime
        
        if mod(rungeStep - 1, rungeToOut) == 0
            vars.ecV(:, rungePoint) = rungeY(:);
            vars.t(rungePoint) = tt;
            rungePoint = rungePoint + 1;
        end
        
        k1 = rungeH * ecSignalling(tt, rungeY);
        k2 = rungeH * ecSignalling(tt + rungeH / 2, rungeY + k1 / 2);
        k3 = rungeH * ecSignalling(tt + rungeH / 2, rungeY + k2 / 2);
        k4 = rungeH * ecSignalling(tt + rungeH, rungeY + k3);
        rungeY = rungeY + (k1 + 2 * k2 + 2 * k3 + k4) / 6;
        tt = tt + rungeH;
        rungeStep = rungeStep + 1;
    end
    
    for j = 1:1:pars.st.vesselsN;
        if strcmp(pars.segment(j).vesselType, 'terminal')
            vars.nephV(pars.segment(j).nephronIdx, n) = vars.ecV(pars.segment(j).lastCellBlock, end);
            vars.nephI(pars.segment(j).nephronIdx, n) = (vars.nephV(pars.segment(j).nephronIdx, n) - pars.ec.restV) * (pars.ec.gapG * double(pars.segment(j).circumferenceCells));
            vars.nephPSI(pars.segment(j).nephronIdx, n) = (vars.nephV(pars.segment(j).nephronIdx, n) - pars.ec.restV);
        end
    end
    
end

coeffeicientsX3 = vars.nephPSI;

    function yPrime = ecSignalling(t, y)
        
        yPrime = zeros(pars.st.cellsN, 1);
        
        for jj = 1:1:pars.st.vesselsN;
            
            firstCellBlock = pars.segment(jj).firstCellBlock;
            lastCellBlock = pars.segment(jj).lastCellBlock;
            parentGapG = pars.segment(jj).parentGapG;
            sisterGapG = pars.segment(jj).sisterGapG;
            daughter1GapG = pars.segment(jj).daughter1GapG;
            daughter2GapG = pars.segment(jj).daughter2GapG;
            circumferenceCells = pars.segment(jj).circumferenceCells;
            
            switch pars.segment(jj).vesselType
                
                case 'root'
                    daughter1FirstCellBlock = pars.segment(pars.segment(jj).daughter1Idx).firstCellBlock;
                    daughter2FirstCellBlock = pars.segment(pars.segment(jj).daughter2Idx).firstCellBlock;
                    
                    parentCurrent = (pars.ec.restV - y(firstCellBlock)) * parentGapG;
                    sisterCurrent = (pars.ec.restV - y(firstCellBlock)) * sisterGapG;
                    
                    daughter1Current = (y(daughter1FirstCellBlock) - y(lastCellBlock)) * daughter1GapG;
                    daughter2Current = (y(daughter2FirstCellBlock) - y(lastCellBlock)) * daughter2GapG;
                    
                case 'terminal'
                    nephronIdx = pars.segment(jj).nephronIdx;
                    parentLastCellBlock = pars.segment(pars.segment(jj).parentIdx).lastCellBlock;
                    sisterFirstCellBlock = pars.segment(pars.segment(jj).sisterIdx).firstCellBlock;
                    
                    parentCurrent = (y(parentLastCellBlock) - y(firstCellBlock)) * parentGapG;
                    sisterCurrent = (y(sisterFirstCellBlock) - y(firstCellBlock)) * sisterGapG;
                    
                    daughter1Current = (vars.nephV0(nephronIdx) - y(lastCellBlock)) * (pars.ec.gapG * circumferenceCells); % 0;%vars.nephI0(nephronIdx);%
                    daughter2Current = 0;
                    
                case 'sub'
                    parentLastCellBlock = pars.segment(pars.segment(jj).parentIdx).lastCellBlock;
                    sisterFirstCellBlock = pars.segment(pars.segment(jj).sisterIdx).firstCellBlock;
                    daughter1FirstCellBlock = pars.segment(pars.segment(jj).daughter1Idx).firstCellBlock;
                    daughter2FirstCellBlock = pars.segment(pars.segment(jj).daughter2Idx).firstCellBlock;
                    
                    parentCurrent = (y(parentLastCellBlock) - y(firstCellBlock)) * parentGapG;
                    sisterCurrent = (y(sisterFirstCellBlock) - y(firstCellBlock)) * sisterGapG;
                    daughter1Current = (y(daughter1FirstCellBlock) - y(lastCellBlock)) * daughter1GapG;
                    daughter2Current = (y(daughter2FirstCellBlock) - y(lastCellBlock)) * daughter2GapG;
                    
                otherwise
                    oneVesselException = MException('PsiMatrix:badStructure', ' Vessel type is not determined');
                    throw(oneVesselException);
                    
            end
            
            for ii = firstCellBlock:1:lastCellBlock
                
                if ii == firstCellBlock
                    yPrime(ii) = yPrime(ii) + parentCurrent + sisterCurrent;
                end
                
                if ii == lastCellBlock
                    yPrime(ii) = yPrime(ii) + daughter1Current + daughter2Current;
                end
                
                if ii > firstCellBlock
                    yPrime(ii) = yPrime(ii) + (y(ii - 1) - y(ii)) * (pars.ec.gapG * circumferenceCells);
                end
                
                if ii < lastCellBlock
                    yPrime(ii) = yPrime(ii) + (y(ii + 1) - y(ii)) * (pars.ec.gapG * circumferenceCells);
                end
                
                % Multiplication of cell current by segment widht moved
                % here
                
                % Possible bug- why minus???? From Dads model
                
                yPrime(ii) = (yPrime(ii) + ecCurrent(y(ii)) * (- circumferenceCells)) / (pars.ec.c * circumferenceCells);
                
            end
            % yPrime(1)=0;
            
        end
        
    end

    function cellCurrent = ecCurrent(cellVoltage)
        
        % Linear approximation
        cellCurrent = (cellVoltage - pars.ec.restV) * pars.ec.restG;
        
        %         %%Piecewise approximation
        %         cellCurrent=(cellVoltage-pars.ec.restV)/(pars.ec.restR);
        %         if (cellVoltage>pars.ec.posV)
        %         cellCurrent=(pars.ec.posV-pars.ec.restV)/(pars.ec.restR)+(cellVoltage-pars.ec.posV)/(pars.ec.posR);
        %         end
        %         if (cellVoltage<pars.ec.negV)
        %         cellCurrent=(pars.ec.negV-pars.ec.restV)/(pars.ec.restR)+(cellVoltage-pars.ec.negV)/(pars.ec.negR);
        %         end
        %
        %
        %         %%Two-component cell current of bistable cell - Postnov et. al. BMB
        %         %%2014
        %         kirW=(0.5/pars.ec.kirDelta)*(pars.ec.kirDelta+ abs(cellVoltage-pars.ec.kirH)-abs(cellVoltage-pars.ec.kirH+pars.ec.kirDelta));
        %         cellCurrent=(1/pars.ec.restR)*(cellVoltage-pars.ec.restV)+kirW*pars.ec.kirG*(cellVoltage-pars.ec.kirV);
        
        % Multiplication of cell current by segment widht moved to the primes
        % description
        
    end

end
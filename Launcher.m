%clear all; clc; close all;
% load('twoVesMatr.mat');
% tic
% pS = LoadModel(nodesMatrix, vesselsMatrix); % LoadModelWithX3(nodesMatrix,vesselsMatrix,x3Matrix);% LoadModel(nodesMatrix,vesselsMatrix);
% x3Matrix = pS.Vasc.coefficientsX3;
% disp([' time ', num2str(toc)]);
% steps = 5;
% Res = repmat(pS, steps, 1);
% disp([' time ', num2str(toc)]);

for i = 1:steps
    S = struct;
    disp(['Worker ', num2str(i)]);
    % S=LoadModel(nodesMatrix,vesselsMatrix);
    S = LoadModelWithX3(nodesMatrix, vesselsMatrix, x3Matrix);
    Res(i) = RunModel(S, i);
    
end

for i = 1:steps
    mfig(strcat('Input pressure ',num2str(Res(i).Neph.S.Hem.pIn)))
    plot(Res(i).Res.y(1:6:2 * 6, :)')
    xlabel('time')
    ylabel('pressure')
end

%figure;plot(Res.Res.y(14));

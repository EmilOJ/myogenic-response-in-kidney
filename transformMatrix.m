function transformedMatrix = transformMatrix(matrix, type)

switch type
    case 'vesselsMatrix'
        transformedMatrix = zeros(length(matrix(:, 1)), 9);
        transformedMatrix(:, 1) = int32(matrix(:, 1));
        transformedMatrix(:, 2) = int32(matrix(:, 2));
        transformedMatrix(:, 3) = matrix(:, 3);
        transformedMatrix(:, 4) = matrix(:, 4);
        transformedMatrix(:, 5) = int32(matrix(:, 5));
        
        transformedMatrix(:, 7) = int32(matrix(:, 6));
        transformedMatrix(:, 8) = int32(matrix(:, 7));
        
        for k = 1:1:length(matrix(:, 1))
            
            if matrix(k, 5) == - 1
                transformedMatrix(k, 6) = - 1;
            else
                indexes = find ((matrix(:, 5) == matrix(k, 5)), 2, 'first');
                if indexes(1) ~= k
                    transformedMatrix(k, 6) = int32(matrix(indexes(1), 1));
                else
                    transformedMatrix(k, 6) = int32(matrix(indexes(2), 1));
                    
                end
            end
            
            if (matrix(k, 6) == - 1 && matrix(k, 7) == - 1)
                transformedMatrix(k, 9) = int32(matrix(k, 1));
            else
                transformedMatrix(k, 9) = int32(- 1);
            end
            
        end
        
    otherwise
end
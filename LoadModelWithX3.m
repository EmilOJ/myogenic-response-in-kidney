function S = LoadModelWithX3(nodesMatrix, vesselsMatrix, x3Matrix)
    format long;
    % Tree size and model paramteres
    S.Tree.vesselsN = length(vesselsMatrix(:, 1));
    S.Tree.nodesN = length(nodesMatrix(:, 1));

    S.Tree.vesselEq = 0;
    S.Tree.nodeEq = 4;
    S.Tree.nephronEq = 6;

    S.Tree.nodesMatrix = nodesMatrix;
    S.Tree.vesselsMatrix = vesselsMatrix;

    % Set interconnections
    for k = 1:1:S.Tree.nodesN

        cn1idx = find (S.Tree.nodesMatrix(:, 1) == S.Tree.nodesMatrix(k, 3), 1, 'first');
        cn2idx = find (S.Tree.nodesMatrix(:, 1) == S.Tree.nodesMatrix(k, 4), 1, 'first');
        cn3idx = find (S.Tree.nodesMatrix(:, 1) == S.Tree.nodesMatrix(k, 5), 1, 'first');
        cv1idx = find (S.Tree.vesselsMatrix(:, 1) == S.Tree.nodesMatrix(k, 6), 1, 'first');
        cv2idx = find (S.Tree.vesselsMatrix(:, 1) == S.Tree.nodesMatrix(k, 7), 1, 'first');
        cv3idx = find (S.Tree.vesselsMatrix(:, 1) == S.Tree.nodesMatrix(k, 8), 1, 'first');

        if (isempty(cn1idx))
            S.Tree.nodesMatrix(k, 11) = - 1;
        else
            S.Tree.nodesMatrix(k, 11) = cn1idx;
        end

        if (isempty(cn2idx))
            S.Tree.nodesMatrix(k, 12) = - 1;
        else
            S.Tree.nodesMatrix(k, 12) = cn2idx;
        end

        if (isempty(cn3idx))
            S.Tree.nodesMatrix(k, 13) = - 1;
        else
            S.Tree.nodesMatrix(k, 13) = cn3idx;
        end

        if (isempty(cv1idx))
            S.Tree.nodesMatrix(k, 14) = - 1;
        else
            S.Tree.nodesMatrix(k, 14) = cv1idx;
        end

        if (isempty(cv2idx))
            S.Tree.nodesMatrix(k, 15) = - 1;
        else
            S.Tree.nodesMatrix(k, 15) = cv2idx;
        end

        if (isempty(cv3idx))
            S.Tree.nodesMatrix(k, 16) = - 1;
        else
            S.Tree.nodesMatrix(k, 16) = cv3idx;
        end

    end

    % RECHECK IT!!!
    counter = 1;
    for k = 1:1:S.Tree.vesselsN
        if (vesselsMatrix(k, 2) == 2)
            S.Tree.nephronsMatrix(counter, 1) = vesselsMatrix(k, 1);
            cnidx = find ((S.Tree.nodesMatrix(:, 7) == S.Tree.nephronsMatrix(counter, 1) | S.Tree.nodesMatrix(:, 8) == S.Tree.nephronsMatrix(counter, 1)), 1, 'first');
            S.Tree.nephronsMatrix(counter, 2) = cnidx;
            cvidx = find ((S.Tree.vesselsMatrix(:, 1) == S.Tree.nephronsMatrix(counter, 1)), 1, 'first');
            S.Tree.nephronsMatrix(counter, 3) = cvidx;
            counter = counter + 1;
        end
    end

    S.Tree.nephronsN = length(S.Tree.nephronsMatrix(:, 1));

    % Tree blood parameters
    S.Hem.pIn = 5.5; % 13.3;%11.25; %artery border condition
    S.Hem.elast = 3000; % Elasticity

    % Electrical coupling
    S.Vasc.resistivity = 10;
    S.Vasc.rLeak = 0.1;
    S.Vasc.rNeph = 1;
    S.Vasc.current = 1;
    S.Vasc.wallWidth = 0.25; % fraction of inner diameter (outDiameter=(1+wallWidth)*innerDiameter
    % S.Vasc.psiMatrix=GetPsiMatrix(S);
    S.Vasc.coefficientsX3 = x3Matrix;

    % Get vascular mechanical resistance
    for k = 1:1:S.Tree.vesselsN;
        S.Hem.length(k) = vesselsMatrix(k, 3);
        S.Hem.diameter(k) = vesselsMatrix(k, 4);
        S.Hem.anatomic_diameter(k) = S.Hem.diameter(k);
        S.Hem.visc(k) = (1 + (6 * exp(- 0.085 * S.Hem.diameter(k)) + 2.2 - 2.44 * exp(- 0.06 * (S.Hem.diameter(k) ^ 0.645))) * ((S.Hem.diameter(k) / (S.Hem.diameter(k) - 1.1)) ^ 2)) * ((S.Hem.diameter(k) / (S.Hem.diameter(k) - 1.1)) ^ 2) / 1000;
        S.Hem.resistance(k) = (128 * S.Hem.visc(k) * S.Hem.length(k)) / (pi * (S.Hem.diameter(k) ^ 4));
        
        %calculate parameters for myogenic response model
        x=S.Hem.diameter(k);
        S.Hem.Ca(k)=(0.007995*x + 0.1087)/1000;
        S.Hem.Cp(k)=(0.006747*x + -0.08029)/1000;
        S.Hem.Ca2(k)=0.91;
        S.Hem.Ca3(k)=0.374;
        S.Hem.ta(k)= 8.56e-09*x^4 + (-6.33e-06)*x^3 + 0.001042*x^2 + 0.1474*x + 0;

        
        
        S.Hem.Cp2(k)= 27.52*exp(-0.04041*x) + 16.41*exp(-0.001273*x);
        S.Hem.Ct(k)= (72.52*exp(-0.01008*x))*1000;
        S.Hem.Ct2(k)= -5.565e-10*x^4 + 6.782e-07*x^3 + (-0.0003134*x^2) + 0.05204*x + 2.057;
        S.Hem.CNo(k)= (2.93e-12*x^5 + (-3.312e-09*x^4) + 1.271e-06*x^3 + (-0.0001745*x^2) + 0.003177*x + 0.9514)*1000;
        S.Hem.CNo2(k)= -2.262e-11*x^5 + 3.42e-08*x^4 + (-1.734e-05*x^3) + 0.00356*x^2 + (-0.2707*x) + 9.586;
        S.Hem.alfa(k)= -2.601e-10*x^4 + 2.871e-07*x^3 + (-0.0001173*x^2) + 0.01842*x + 0.05665;
    end

    % single nephron variable staff =========================
    % this parameters we do change
    S.Neph.t = (13.5); % Henle's loop delay etc. [s]%
    S.Neph.alpha = (13.0); % Transglomerular feedback amplification [1]%
    S.Neph.pRef = (1.3);
    S.Neph.pTau = (.1);
    S.Neph.pScale = (.035);
    S.Neph.pIn = 0; % Arterial blood preasure [kPa]%

    % this are usually constants
    S.Neph.cTub = (3.0); % Elasticity [nL/kPa]%
    S.Neph.ha = (0.5); % Arterial hematochrite (?) [1]%
    S.Neph.pEf = (1.3); % Efferent arterial pressure [kPa]%
    S.Neph.pDist = (0.6); % CDistale tubulus hydrostatic pressure [kPa]%
    S.Neph.fHenEq = (0.2); % Henle's loop equilibrium flow [nL/s]%
    S.Neph.fReab = (0.3); % Proximate tubulus reabsorption [nL/s]%
    S.Neph.rHen = (5.3); % Henle's loop resistance [kPa*s/nL]%

    S.Neph.rAffEq = (2.3); % Afferent arterial equilibrium resistance [kPa*s/nL]%

    S.Neph.rEff = (1.9); % Efferent arterial resistance [kPa*s/nL]%
    S.Neph.omega = (20.0); % Damped oscillator parameter [kPa*s^2]%
    S.Neph.d = (0.04); % ! DISCREPANCY - Damped oscillator parameter [1/s]%
    S.Neph.beta = (0.67); % Non-variable fraction of efferent arterial [1]%
    S.Neph.psiMin = (0.20); % Lower activation limit [1]%
    S.Neph.psiMax = (0.44); % Upper activation limit [1]%
    S.Neph.psiEq = (0.38); % Equilibrium activation [1]%
    S.Neph.cVasc = (3.0); % Elasticity [nL/kPa]%

    % Parameters used only in cubic equation, therefore units [L] and [g] are OK%
    S.Neph.cAff = (54.0); % Afferent plasma protein concentration [g/L]%
    S.Neph.a = (22.0e-3); % Protein concentration parameter [kPa*L/g]%
    S.Neph.b = (0.39e-3); % Protein concentration parameter [kPa*L^2/g^2]%

    % Simulation parameters
    S.Model.points = 250;
    S.Model.tSpan = [0 250];
    S.Model.t = linspace(S.Model.tSpan(1), S.Model.tSpan(2), S.Model.points);

    % Set initial conditions
    S.Model.yZero = zeros(S.Tree.nephronEq * S.Tree.nephronsN + S.Tree.nodesN * S.Tree.nodeEq + S.Tree.vesselsN * S.Tree.vesselEq, 1);
    previousEq = 0;
    for i = previousEq + 1:S.Tree.nephronEq:previousEq + S.Tree.nephronEq * (S.Tree.nephronsN - 1) + 1
        S.Model.yZero(i) = 2.07828; % - (rand*0.1-0.05);
        S.Model.yZero(i + 1) = 1.1891; % - (rand*0.1-0.05);
        S.Model.yZero(i + 2) = - 0.186993; % - (rand*0.1-0.05);
        S.Model.yZero(i + 3) = 1.44853; % - (rand*0.1-0.05);
        S.Model.yZero(i + 4) = 1.25917; % - (rand*0.1-0.05);
        S.Model.yZero(i + 5) = 1.0752; % - (rand*0.1-0.05);
    end
    previousEq = S.Tree.nephronEq * S.Tree.nephronsN;
    for i = previousEq + 1:S.Tree.nodeEq:previousEq + (S.Tree.nodesN - 1) * S.Tree.nodeEq + 1
        S.Model.yZero(i) = S.Hem.pIn-1;
        S.Model.yZero(i + 1) = 0.1;
        S.Model.yZero(i + 2) = S.Hem.anatomic_diameter(1);
        S.Model.yZero(i + 3) = 5e10;
    end

    S.Res.t = S.Model.t;
    S.Res.y = zeros(S.Tree.nephronEq * S.Tree.nephronsN + S.Tree.nodesN * S.Tree.nodeEq + S.Tree.vesselsN * S.Tree.vesselEq, S.Model.points);

end

function modelGUI(Res)
steps=size(Res,1);
modelFig=figure('Visible', 'on', 'Position', [150, 250, 1000, 650],...
        'Name','-- Model Inspector  --');

graphPanel = uitabgroup(modelFig,'Position',[.025,.025,.95,.95]);


%% initiations
colorMatrix={'red','blue','green','magenta'};


%% Graph of Diameter

tab1 = uitab(graphPanel,'Title','diameter');

diaGraph= axes('Parent',tab1,'Units','Pixels','Position',[40,40,650,500]);

%% Graph of response

tab2 = uitab(graphPanel,'Title','Response');

resGraph=axes('Parent',tab2,'Units','Pixels','Position',[40,40,650,500]);
%% Options Diameter
%optionPanel = uipanel('Title','Options','Fontsize',10,'Position',[.775,.025,.2,.95]);
uicontrol('Parent',tab1,'Style','text','String','# Initial pressures:',...
            'Position',[770 530 100 20],'Background','white');
uicontrol('Parent',tab1,'Style','text','String',num2str(size(Res,1)),...
            'Position',[890 530 20 20],'Background','white');
uichangepres= uicontrol('Parent',tab1,'Style', 'popup',...
           'Position', [770 480 150 30],...
           'Callback', @changepres);   
%% Options Response
uicontrol('Parent',tab2,'Style','text','String',{'nephron1','nephron2','node','total mean'},...
            'Position',[770 470 100 80],'Background','white','FontSize',18);
btn1 = uicontrol('Parent',tab2,'Style','checkbox','Position',[870 530 20 20],...
    'Value',1);
btn2 = uicontrol('Parent',tab2,'Style','checkbox','Position',[870 510 20 20],...
    'Value',1);
btn3 = uicontrol('Parent',tab2,'Style','checkbox','Position',[870 490 20 20],...
    'Value',1);
btn4 = uicontrol('Parent',tab2,'Style','checkbox','Position',[870 470 20 20],...
    'Value',1);
uicontrol('Parent',tab2,'Style','pushbutton','String','Update','Position',...
            [770 440 100 20],'Callback',@updateGraphFn);
%% Initiate
% plot
    plot(Res(1).Res.y(1 * 6, :)','Parent',diaGraph);
    xlabel('time','Parent',diaGraph)
    ylabel('pressure','Parent',diaGraph)
% presuretab
string1={''};
for i=1:steps
    string1(i)={num2str(Res(i).Neph.S.Hem.pIn)};
end
set(uichangepres,'String',string1)
    
% responsetab
meanPres1=[];
meanPres2=[];
meanPres3=[];

inPres=[];
for i=1:steps
    meanPres1(i)=mean(Res(i).Res.y(1, 500:end));
    meanPres2(i)=mean(Res(i).Res.y(7, 500:end));
    meanPres3(i)=mean(Res(i).Res.y(7, 500:end));
    inPres(i)=Res(i).Neph.S.Hem.pIn;
end
meanPres4=mean([meanPres1;meanPres2;meanPres3]);
hold(resGraph,'on')
plot(resGraph,inPres,meanPres1,cell2mat(colorMatrix(1)));
plot(resGraph,inPres,meanPres2,cell2mat(colorMatrix(2)));
plot(resGraph,inPres,meanPres3,cell2mat(colorMatrix(3)));
plot(resGraph,inPres,meanPres4,cell2mat(colorMatrix(4)));
legend(resGraph,'nephron1','nephron2','node','total mean')
hold(resGraph,'on')

    xlabel('Initial Pressure','Parent',resGraph)
    ylabel('mean pressure in afferent arterioles','Parent',resGraph)
%% functions
    function changepres(source,eventdata)
        plot(Res(source.Value).Res.y(1:6:2 * 6, :)','Parent',diaGraph);
    end

    function updateGraphFn(source,eventdata)
        graphs=[get(btn1,'Value'),get(btn2,'Value'),get(btn3,'Value'),get(btn4,'Value')];
        cla(resGraph)
        hold(resGraph,'on')
        for j=1:4
           if graphs(j)==1
               plot(resGraph,inPres,eval(strcat('meanPres',num2str(j))),cell2mat(colorMatrix(j)));
           end
        end
        hold(resGraph,'off')
    end
end
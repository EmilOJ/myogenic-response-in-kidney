clear all; close all; clc;

%Time steps
S.t_span    = linspace(0, 1000, 1e4);
order = 8;

% Parameters read from constants.m
eval('constants');

% S.Ca=Ca(order);
% S.Ca2=Ca2(order);
% S.Ca3=Ca3(order);
% S.CNo=CNo(order);
% S.CNo2=CNo2(order);
% S.Cp=Cp(order);
% S.Cp2=Cp2(order);
% S.Ct=Ct(order);
% S.Ct2=Ct2(order);
% S.ta=ta(order);
% S.alfa=alfa(order);
S.anatomic_diameter=21.5;
S.diameter=S.anatomic_diameter/2;
%calculate parameters for myogenic response model
        x=S.anatomic_diameter;
        S.Ca=(0.007995*x + 0.1087)/1000;
        S.Cp=(0.006747*x + -0.08029)/1000;
        S.Ca2=0.91;
        S.Ca3=0.374;
        S.ta=8.56e-09*x^4 + (-6.33e-06)*x^3 + 0.001042*x^2 + 0.1474*x + 0;
        S.Cp2= 27.52*exp(-0.04041*x) + 16.41*exp(-0.001273*x);
        S.Ct= (72.52*exp(-0.01008*x))*1000;
        S.Ct2= -5.565e-10*x^4 + 6.782e-07*x^3 + (-0.0003134*x^2) + 0.05204*x + 2.057;
        S.CNo= (2.93e-12*x^5 + (-3.312e-09*x^4) + 1.271e-06*x^3 + (-0.0001745*x^2) + 0.003177*x + 0.9514)*1000;
        S.CNo2= -2.262e-11*x^5 + 3.42e-08*x^4 + (-1.734e-05*x^3) + 0.00356*x^2 + (-0.2707*x) + 9.586;
        S.alfa= -2.601e-10*x^4 + 2.871e-07*x^3 + (-0.0001173*x^2) + 0.01842*x + 0.05665;


S.numberOfVessels = numberOfVessels(order);
S.vesselLength=vesselLength(order);
%S.anatomic_diameter=anatomic_diameter(order);
S.yzero = 0.1; %Myogenic activation initial

tic
S = run_model(S); 
toc
%%
close all;
fontsize = 35;
subplots = 3;
%Plot of activation
figure; subplot(subplots,1,1);
plot(S.t_span, S.y(1,:));
ylabel('\Phi_{MR}', 'FontSize',fontsize);
xlabel('time', 'FontSize',fontsize);

%TODO: Recompute diameter from solution
subplot(subplots,1,2); plot(S.t_span, S.y(2,:));
ylabel('diameter', 'FontSize',fontsize);
xlabel('time', 'FontSize',fontsize);

% subplot(subplots,1,3); plot(S.t_span, S.y(3,:));
% ylabel('max active tenstion', 'FontSize',fontsize);
% xlabel('time', 'FontSize',fontsize);

subplot(subplots,1,3); plot(S.t_span, S.y(4,:));
ylabel('Flow', 'FontSize',fontsize);
xlabel('time', 'FontSize',fontsize);


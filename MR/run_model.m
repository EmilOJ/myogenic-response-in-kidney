
function S = run_model(S)

sol = ode45(@diff_equ, S.t_span, [S.yzero,S.anatomic_diameter, 2.7, 5e7, 0]);
S.y = deval(sol, S.t_span);



function yPrime = diff_equ(t, y)
    
    t;

    %Linearly increasing pressure
    if t<200
    P = 10*133/1000;
    elseif t<600
            P=30*133/1000;
    else
      P=60*133/1000;
    end
  % P=20*133;
    
   % Solving for diameter
    fun = @diameter_eq;
    x0 = S.anatomic_diameter;
    options = optimset('Display','off');
    [diameter, fval] = fsolve(fun,x0,options);
    
    
    
    %diameter=findDia(S.Cp,S.Cp2,S.Ca, S.anatomic_diameter,S.numberOfVessels,S.vesselLength, S.Ct,S.CNo2,S.Ct2,S.Ca2,S.Ca3,P,S.alfa,S.CNo,y(1));
    function F = diameter_eq(diameter)
        
        visc = ((1 + (6 * exp(- 0.085 * diameter) + 2.2 - 2.44 ...
        * exp(- 0.06 * (diameter ^ 0.645))) ...
        * ((diameter / (diameter - 1.1)) ^ 2)) ...
        * ((diameter / (diameter - 1.1)) ^ 2));


        %Unit conversion: kPa*s
        visc = visc / 1e6;

        %Unit: Pa*s/micro cubic meters
        resistance    = (128 * visc * S.vesselLength) / (S.numberOfVessels * pi * (diameter ^ 4));
        
        %Flow
        %Unit: micro cubic meters / second
        Q = 133*1e-3 / resistance;



        dnorm = diameter/ S.anatomic_diameter;

        %Equation 7
        %Unit: N/m
        Tpass = S.Cp * exp(S.Cp2 * (dnorm - 1));

        %Equation 8
        %Unit: N/m
        Tmax = S.Ca * exp(- ((dnorm - S.Ca2) / S.Ca3) ^ 2);
        %Equation 6
        %Unit: N/m
        Ttot = Tpass + y(1) * Tmax;

        %Equation 9
        %Unit: kPa
        tau = (32 * Q * visc / (S.numberOfVessels * pi * diameter ^ 3));

        %Equation 10
        %Unit: Unitless (percentage activation)
        phiNo = 1 / (1 + exp(- (S.CNo * tau - S.CNo2)));

        %Equation 11
        %Unit: Unitless (percentage activation)
        phiMRss = 1 / (1 + exp(- ((S.Ct * Ttot) / (1 + S.alfa * phiNo) - S.Ct2)));
        
        F = (S.Cp  * exp(S.Cp2 * (dnorm - 1)) + S.Ca * phiMRss * exp(-((dnorm - S.Ca2) / S.Ca3)^2))*10^6 - (P * diameter / 2);
    end
        
            
        
    %Unit: Centipoise (=mPa*s)
    visc = ((1 + (6 * exp(- 0.085 * diameter) + 2.2 - 2.44 ...
        * exp(- 0.06 * (diameter ^ 0.645))) ...
        * ((diameter / (diameter - 1.1)) ^ 2)) ...
        * ((diameter / (diameter - 1.1)) ^ 2));


%     %Unit conversion: Pa*s
%     visc = visc / 1000;
% 
%     %Unit: Pa*s/micro cubic meters
%     resistance    = (128 * visc * S.vesselLength) / (S.numberOfVessels * pi * (diameter ^ 4));
% 
%     %Unit: micro cubic meters / second
%     Q = 133 / resistance;
% 
% 
% 
%     dnorm = diameter/ S.anatomic_diameter;
% 
%     %Equation 7
%     %Unit: N/m
%     Tpass = S.Cp * exp(S.Cp2 * (dnorm - 1));
% 
%     %Equation 8
%     %Unit: N/m
%     Tmax = S.Ca * exp(- ((dnorm - S.Ca2) / S.Ca3) ^ 2);
%     %Equation 6
%     %Unit: N/m
%     Ttot = Tpass + y(1) * Tmax;
% 
%     %Equation 9
%     %Unit: Pa
%     tau = (32 * Q * visc / (S.numberOfVessels * pi * diameter ^ 3));
% 
%     %Equation 10
%     %Unit: Unitless (percentage activation)
%     phiNo = 1 / (1 + exp(- (S.CNo * tau - S.CNo2)));
% 
%     %Equation 11
%     %Unit: Unitless (percentage activation)
%     phiMRss = 1 / (1 + exp(- ((S.Ct * Ttot) / (1 + S.alfa * phiNo) - S.Ct2)));

visc = ((1 + (6 * exp(- 0.085 * diameter) + 2.2 - 2.44 ...
        * exp(- 0.06 * (diameter ^ 0.645))) ...
        * ((diameter / (diameter - 1.1)) ^ 2)) ...
        * ((diameter / (diameter - 1.1)) ^ 2));


        %Unit conversion: kPa*s
        visc = visc / 1e6;

        %Unit: Pa*s/micro cubic meters
        resistance    = (128 * visc * S.vesselLength) / (S.numberOfVessels * pi * (diameter ^ 4));
        
        %Flow
        %Unit: micro cubic meters / second
        Q = 133*1e-3 / resistance;



        dnorm = diameter/ S.anatomic_diameter;

        %Equation 7
        %Unit: kN/m
        Tpass = S.Cp * exp(S.Cp2 * (dnorm - 1));

        %Equation 8
        %Unit: kN/m
        Tmax = S.Ca * exp(- ((dnorm - S.Ca2) / S.Ca3) ^ 2);
        %Equation 6
        %Unit: kN/m
        Ttot = Tpass + y(1) * Tmax;

        %Equation 9
        %Unit: kPa
        tau = (32 * Q * visc / (S.numberOfVessels * pi * diameter ^ 3));

        %Equation 10
        %Unit: Unitless (percentage activation)
        phiNo = 1 / (1 + exp(- (S.CNo * tau - S.CNo2)));

        %Equation 11
        %Unit: Unitless (percentage activation)
        phiMRss = 1 / (1 + exp(- ((S.Ct * Ttot) / (1 + S.alfa * phiNo) - S.Ct2)));


    %Equation 13
    yPrime(1,1) = 1 / S.ta * (phiMRss - y(1));
    yPrime(2,1) = diameter - y(2);
    yPrime(3,1) = Tmax - y(3);
    yPrime(4,1) = Q - y(4);
    yPrime(5,1)= visc - y(5);

end
    
        
end
% function k=findDia(Cp,Cp2,Ca, anatomic_diameter,numberOfVessels,vesselLength, Ct,CNo2,Ct2,Ca2,Ca3,P,alfa,CNo,y)
%         syms x
%     k= double(vpasolve( (((Cp  * exp(Cp2 * ((x/ anatomic_diameter) - 1)) + Ca * (1 / (1 + exp(- ((Ct * ((Cp * exp(Cp2 * ((x/ anatomic_diameter) - 1))) + y * (Ca * exp(- (((x/ anatomic_diameter) - Ca2) / Ca3) ^ 2)))) / (1 + alfa * (1 / (1 + exp(- (CNo * ((32 * (133 / ((128 * (((1 + (6 * exp(- 0.085 * x) + 2.2 - 2.44 ...
%         * exp(- 0.06 * (x ^ 0.645))) ...
%         * ((x / (x - 1.1)) ^ 2)) ...
%         * ((x / (x - 1.1)) ^ 2)) / 1000) * vesselLength) / (numberOfVessels * pi * (x^ 4)))) * (((1 + (6 * exp(- 0.085 * x) + 2.2 - 2.44 ...
%         * exp(- 0.06 * (x ^ 0.645))) ...
%         * ((x / (x - 1.1)) ^ 2)) ...
%         * ((x / (x - 1.1)) ^ 2)) / 1000) / (numberOfVessels * pi * x ^ 3))) - CNo2))))) - Ct2)))) * exp(-(((x/ anatomic_diameter) - Ca2) / Ca3)^2))*10^6)*2)/P == x, x));
%     
%     end